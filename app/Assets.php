<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
      protected $fillable=[
        'id',
        'customer_id',
          'category',
        'name',
        'price',
        'purchased',

    ];
    
    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
