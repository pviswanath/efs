<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Assets;
use App\Customer;

class AssetsController extends Controller
{
        public function index()
    {
        //
        $assets=Assets::all();
        return view('assets.index',compact('assets'));
    }

    public function show($id)
    {
        
        $assets= Assets::findOrFail($id);

        return view('assets.show',compact('assets'));
    }


    public function create()
    {

        $customers = Customer::lists('name','id');
        return view('assets.create', compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)   
    {

       $assets= new Assets($request->all());
       $assets->save();

              return redirect('assets');
    }

    public function edit($id)
    {
        $assets=Assets::find($id);
        return view('assets.edit',compact('assets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {

        $assets= new Assets($request->all());
        $assets=Assets::find($id);
        $assets->update($request->all());
        return redirect('assets');
    }

    public function destroy($id)
    {
        Assets::find($id)->delete();
        return redirect('assets');
    }

}
