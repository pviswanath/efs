@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"> Assets</div>
     <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr class="bg-info">
            <tr>
                <td>Assets Category</td>
                <td><?php echo ($assets['Id']); ?></td>
            </tr>
            <tr>
                <td>Assets Name</td>
                <td><?php echo ($assets['category']); ?></td>
            </tr>
            <tr>
                <td>Assets Name</td>
                <td><?php echo ($assets['name']); ?></td>
            </tr>
            
            <tr>
                <td>Price </td>
                <td><?php echo ($assets['price']); ?></td>
            </tr>
            <tr>
                <td>Purchased</td>
                <td><?php echo ($assets['purchased']); ?></td>
            </tr>
            </tbody>
        </table>
    </div>
                </div>
            </div>
        </div>
    </div>
@stop

