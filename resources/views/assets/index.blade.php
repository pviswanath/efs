@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Assets</div>
                    <a href="{{url('/assets/create')}}" class="btn btn-success">Create Assets</a>
                    <hr>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr class="bg-info">
                            <th>Cust No</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th>date</th>
                             <th colspan="3">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($assets as $asset)
                            <tr>
                                <td><?php echo ($asset['customer_id']); ?></td>
                                <td><?php echo ($asset['category']); ?></td>
                                <td><?php echo ( $asset ['name']); ?></td>
                                <td><?php echo ($asset['price']); ?> </td>
                                <td><?php echo ( $asset['purchased']); ?></td>

                                <td><a href="{{url('assets',$asset->id)}}" class="btn btn-primary">Read</a></td>
                                <td><a href="{{route('assets.edit',$asset->id)}}" class="btn btn-warning">Update</a></td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE', 'route'=>['assets.destroy', $asset->id]]) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

